print("Loading RPG...")
import random
import json
import time

class PARTYMANAGER:
    def __init__(self):
        self.party = []
    
    def addCharacter(self, characterObject):
        self.party.append(characterObject)
        return True

    # Below 2 functions relate to Issue #3
    def save(self, filename):
        try:
            partyJSON = json.dumps(self.party)
            with open(filename, 'w') as f:
                f.write(partyJSON)
            return [True, None]
        except Exception as e:
            return [False, e]
    
    def load(self, filename):
        try:
            partyJSON = ""
            with open(filename, 'r') as f:
                partyJSON = f.read()
            self.party = json.loads(partyJSON)
            return [True, None]
        except Exception as e:
            return [False, e]

def options(validOptions, prompt = ">>>"):
    # This function is to get and validate user choice in one, re-usable function
    option = input(prompt+" ").lower()
    while option not in validOptions:
        print("Sorry, that's not a valid choice! Please choose again.")
        option = input(prompt+" ").lower()
    return option

def speech(name, says, wpm=260):
    # This function prints character speech in a typewriter fashion
    # the 'end=""' means don't put a new line after the print statement
    print(name+"> ", end="")
    for i in says: # for each letter in says
        print(i, end="")
        time.sleep(0.02)
    print("") # add a new line to finish off

    # This code waits some time after the character has finished speaking
    # in order to let the player read what has been said. 
    time.sleep(2)

class RECRUITMENT:
    def generateStat(self):
        diceRolls = [
            random.randint(1,6),
            random.randint(1,6),
            random.randint(1,6),
            random.randint(1,6)
        ]

        # find and drop the lowest roll
        lowest = 0
        for i in range(len(diceRolls)):
            if diceRolls[i] < diceRolls[lowest]:
                lowest = i
        diceRolls.pop(i)

        # add it up
        total = 0
        for i in diceRolls:
            total += i
        return total

    def generateAttributes(self):
        attributes = [
            self.generateStat(),
            self.generateStat(),
            self.generateStat(),
            self.generateStat()
        ]

        # this prints out the attributes and finds the lowest at the same time
        lowest = 0
        print("Your attributes are: ", end="")
        for i in range(len(attributes)-1):
            print(str(attributes[i]), end=", ")
            if attributes[i] < attributes[lowest]:
                lowest = i
        print(" and " + str(attributes[3]) + ".")
        if attributes[3] < attributes[lowest]:
            lowest = 3
        
        print("Your lowest attribute is "+str(attributes[lowest])+". You can re-roll it, if you like, but you can't re-roll twice.")
        choice = options(["y","n"], "Reroll your lowest attribute ([Y]es or [N]o)? ")
        if choice == "y":
            newAttribute = self.generateStat()
            print("Your new attribute is "+str(newAttribute))
            attributes[lowest] = newAttribute
        
        return attributes

    def assignAttributes(self, attributes):
        statTitles = ["strength","agility","magic","luck"]
        stats={}

        for i in range(len(attributes)-1):
            print("Which stat do you want to assign "+str(attributes[i])+" to? Choose from:")
            choices = []
            for j in statTitles:
                choices.append(j[0])
                print("["+j[0].upper()+"]"+j[1:])
            userChoice = options(choices)
            userChoiceIndex = choices.index(userChoice)
            stats[statTitles[userChoiceIndex]] = attributes[i]
            print("Assigned to "+statTitles[userChoiceIndex]+".")
            statTitles.pop(userChoiceIndex)
        
        lastAttribute = attributes[len(attributes)-1]
        stats[statTitles[0]] = lastAttribute
        print("The last attribute, "+str(lastAttribute)+", has been assigned to "+statTitles[0]+".")

        return stats


    def recruitCharacter(self):
        name = input("Choose a name for this character: ")

        print("Ok. Generating attributes for "+name+"...")
        attributes = self.generateAttributes()

        print("You now need to assign these attributes.")
        stats = self.assignAttributes(attributes)

        characterObject = {
            "name": name,
            "hitpoints": 2,
            "stats": stats,
            "fatigued": False
        }

        return characterObject

class CHALLENGEMANAGER:
    def __init__(self):
        statTitles = ["strength","agility","magic","luck"]
        names = ["Alex", "Bjorn", "Charles", "Dave", "Eloise",
                "Fran", "Gemma", "Helen", "Ian", "Jane",
                "Ken", "Lucy", "Michael", "Nia", "Oliver",
                "Pam", "Quentin", "Rita", "Steve", "Theresa",
                "Ursula", "Victor", "Winnifred", "Xavier", "Yvonne",
                "Zoe"]
        self.data = {
            "rating": random.randint(1,15),
            "name": random.choice(names),
            "stat": random.choice(statTitles)
        }

    def engage(self, character):
        characterBoost = random.randint(1,20)
        print(character['name'].upper()+" got a boost of "+str(characterBoost)+"!")
        enemyBoost = random.randint(1,20)
        print(self.data['name'].upper()+" got a boost of "+str(enemyBoost)+"!")
        time.sleep(3)
        characterTotal = character['stats'][self.data['stat']]+characterBoost
        enemyTotal = self.data['rating']+enemyBoost

        preBattleSayings = [
            "Come on then! What are you waiting for?",
            "I've been waiting for this all day!",
            "This is usually the part where you beg for mercy.",
            "May the best man win.",
            "I don't really know how good my fighting technique is. No-one's survived to tell me.",
            "Once more into the breach, dear friends, once more!",
            "Let the duel begin."
        ]
        print("")
        speech(self.data['name'], random.choice(preBattleSayings))

        actions = { # A & B are replaced with the character names!
            "strength": [
                "A punches B!",
                "A kicks B!",
                "A slaps B!",
                "A knocks down B, but B recovers!"
            ],
            "agility":[], # agility always results in a race which has no actions.
            "magic": [
                "A casts a spell to bamboozle B!",
                "A is temporarily hypnotized by B!"
            ],
            "luck": [] # Luck always results in a game of cards which has pre-set actions.
        }

        print("The battle commences!")
        if self.data['stat'] in ["strength","magic"]:
            for i in range(random.randint(3,5)):
                time.sleep(random.randint(20,50)/10)
                action = random.choice(actions[self.data['stat']])
                players = [character['name'],self.data['name']]
                random.shuffle(players)
                action = action.replace("A", players[0]).replace("B", players[1])
                print(action)
        elif self.data['stat'] == "agility":
            print("They decide to have a race.")
            time.sleep(1)
            print("They line up on the start line...")
            time.sleep(2)
            for i in [3,2,1]:
                print(str(i)+"...",end=" ")
                time.sleep(1)
            print("GO!")
            time.sleep(0.5)
            players = [character['name'],self.data['name']]
            random.shuffle(players)
            action = "A reacts just before B..."
            action = action.replace("A", players[0]).replace("B", players[1])
            print(action)
            time.sleep(2)
            action = "B closes the gap - It's neck & neck..."
            action = action.replace("B", players[1])
            print(action)
            time.sleep(1.5)
        elif self.data['stat'] == "luck":
            print("Both contestants pick a card. The highest card wins.")
            low  = ["2","3","4","5"]
            high = ["6","7","8","9","10"]
            time.sleep(2)
            print("They both reveal their cards:")
            time.sleep(0.7)
            characterCard = ""
            enemyCard = ""
            if characterTotal>= enemyTotal:
                characterCard = random.choice(high)
                enemyCard = random.choice(low)
            else:
                characterCard = random.choice(low)
                enemyCard = random.choice(high)
            print(character['name']+" got "+characterCard)
            print(self.data['name']+" got "+enemyCard)



        battleEndSayings = []
        if characterTotal>= enemyTotal:
            print(character['name'].upper()+" (you) WINS!")
            battleEndSayings = [
                "Finally, a contestant worth my time.",
                "You may have defeated me, but this is a cruel world. I wish you luck, traveler.",
                "I surrender. You have the better of me.",
                "My "+self.data['stat']+" is weakened. You're the first to have done so."
            ]
        else:
            print(self.data['name'].upper()+" (the challenger) WINS!")
            battleEndSayings = [
                "Another one bites the dust.",
                "You are weak. I am strong. I am VICTORIOUS!",
                "You fought valiantly, but not valiantly enough.",
                "No-one has beaten my "+self.data['stat']+", and you are no exception!"
            ]
        speech(self.data['name'], random.choice(battleEndSayings))
        return characterTotal>= enemyTotal

def cls():
    for i in range(100): print("") # a simple function to clear the screen


print("===========================================================")
print("                    Welcome to the game!                   ")
print("To choose a menu option, use the letter in square brackets.")
print("===========================================================")
print("                     Begin [N]ew game                      ")
print("                   [L]oad existing party                   ")
print("===========================================================")

choice = options(["n","l"])
partyMan = PARTYMANAGER()

if choice == "l":
    imported = False
    while not imported:
        name = input("Enter party name: ")
        status = partyMan.load(name+".party.json")
        if status[0]:
            print("Your party is ready. Entering the guild...")
            imported = True
        else:
            print("There was an error obtaining the file. Perhaps it doesn't exist? Anyway, please choose another.")
    time.sleep(5)

    if len(partyMan.party) < 4:
        needed = 4 - len(partyMan.party)
        speech("FINN", "It appears you have lost some of your party along the way.")
        speech("FINN", "You need to recruit "+str(needed)+" party members to continue on your journey.")
        speech("FINN", "I shall begin the character recruitment programme.")
elif choice == "n":
    print("Welcome! Taking you into the guild...")
    time.sleep(3)
    cls()
    speech("FINN", "Greetings, new guild member! I am Finn, the guild master.")
    speech("FINN", "You are about to embark on an adventure into the Woods. For this, you will need a party.")
    speech("FINN", "I am about to show you the character recruitment programme. Here you shall fill your party with 4 members of different skills.")
    speech("FINN", "Good luck!")
    input("Press enter to begin the character recruitment programme...")

needed = 4 - len(partyMan.party)
characterNumber = 1
if needed > 0:
    recruitment = RECRUITMENT()
    for i in range(needed):
        print("Recruiting character "+str(characterNumber)+"/"+str(needed)+".")
        character = recruitment.recruitCharacter()
        partyMan.addCharacter(character)
        print("Recruited.")
        characterNumber += 1
        time.sleep(1)

speech("FINN", "Excellent. You have all you need.")
speech("FINN", "Now go forth and adventure!")
input("Press enter to begin the adventure.")

for i in range(5): # for 5 turns
    cls()
    challengeman = CHALLENGEMANAGER()
    challenge = challengeman.data
    print("A challenger approaches!")
    time.sleep(1)
    print("")
    speech(challenge['name'], "I challenge YOU to a BATTLE of "+challenge['stat'].upper()+"!")
    print("This challenge is rated at "+str(challenge['rating'])+"!")
    time.sleep(3)
    print("")
    print("==================================================")
    print("Choose a character to fight "+challenge['name']+":")
    acceptable = []
    characters = []
    for j in partyMan.party:
        characters.append(j['name'].lower())
        print("")
        if not j['fatigued']:
            print(j['name'])
            print("S  | A  | M  | L  | HP ")
            print("---+----+----+----+----")
            print('{0:2d} | {1:2d} | {2:2d} | {3:2d} | {4:2d}'.format(
                j['stats']['strength'], 
                j['stats']['agility'],
                j['stats']['magic'],
                j['stats']['luck'],
                j['hitpoints']
                ))
            acceptable.append(j['name'].lower())
        else:
            print(j['name']+" is resting and cannot fight.")
            j['fatigued'] = False
    print("")
    choice = options(acceptable, "Enter the full name of the character who shall fight: ")
    print("")
    print(choice+" is engaging with "+challenge['name']+" in a battle of "+challenge['stat']+"!")
    time.sleep(3)
    won = challengeman.engage(partyMan.party[characters.index(choice)])
    if won:
        partyMan.party[characters.index(choice)]['fatigued'] = True
        if partyMan.party[characters.index(choice)]['hitpoints'] < 4:
            partyMan.party[characters.index(choice)]['hitpoints'] += 1
            print("Gained 1 HP!")
        else:
            print("Already at 4HP!")
    else:
        partyMan.party[characters.index(choice)]['hitpoints'] -= 1
        print("Lost 1HP.")
        if partyMan.party[characters.index(choice)]['hitpoints'] < 1:
            time.sleep(2)
            print("Sadly, "+partyMan.party[characters.index(choice)]['name']+" passed away.")
            partyMan.party.pop(characters.index(choice))
        else:
            partyMan.party[characters.index(choice)]['fatigued'] = True
    time.sleep(2)
    if i != 4: # if not last challenge
        input("Press enter for your next challenge.")
speech("FINN", "Your adventure is over now. You may rest.")

time.sleep(5)
save = options(['y','n'],"Save your party (y/n)?")
if save == "y":
    partyname=input("Enter a party name: ")
    partyMan.save(partyname+".party.json")
    print("Saved as "+partyname+".")

print("At the end of this adventure, your party looks like this:")
for j in partyMan.party:
    print("")
    print(j['name'])
    print("S  | A  | M  | L  | HP ")
    print("---+----+----+----+----")
    print('{0:2d} | {1:2d} | {2:2d} | {3:2d} | {4:2d}'.format(
        j['stats']['strength'], 
        j['stats']['agility'],
        j['stats']['magic'],
        j['stats']['luck'],
        j['hitpoints']
        ))
    if j['fatigued']: print(j['name']+" is resting and cannot fight.")

input("Thank you for playing. Press enter to close. ")
