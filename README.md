# AGS - RPG game
The RPG game for Jack Dunn's (11F) AGS Sixth Form entry.

## Files
Included in this are 3 files plus this README:
* `rpg.py` - the game itself
* `full.party.json` - a full party save.
* `onedead.party.json` - a party save with one dead character.

## Testing the game
To start a new game, start the python file and select `N`.

To use an existing file save, select `L` and then type the name of the file
EXCLUDING the `.party.json` - e.g. `full.party.json` becomes `full`.

### Saves bundled with the game
To access each of these, select `L` at the main menu.

#### Full party
This is a save that has 4 characters in it with differing stats. To use this one,
enter party name `full`.

#### Party with one missing member
This save only has 3 characters, to show that the program allows the user to re-fill
their party. To use this one, enter party name `onedead`.

